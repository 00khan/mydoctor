<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get("password",function(){
    dd(bcrypt('admin11'));
});
//Clear configurations:
            Route::get('/list', function() {
                $status = Artisan::call('vendor:publish --tag=lfm_public');
                // dd($status);
                return '<h1>Configurations cleared</h1>';
            });

//Clear configurations:
            Route::get('/config-clear', function() {
                $status = Artisan::call('config:clear');
                return '<h1>Configurations cleared</h1>';
            });

//Clear cache:
            Route::get('/cache-clear', function() {
                $status = Artisan::call('cache:clear');
                return '<h1>Cache cleared</h1>';
            });

//Clear configuration cache:
            Route::get('/config-cache', function() {
                $status = Artisan::call('config:cache');
                // dd($status);
                return '<h1>Configurations cache cleared</h1>';
            });
//Run task schduling :
            Route::get('/tasksch', function() {
                $status = Artisan::call('schedule:run');
                // dd($status);
                return '<h1>scheduled commands are ready to run</h1>';
            });

Route::get('d','Frontend\HomeController@download');

Route::get('/','Frontend\HomeController@index');

Route::get('search-by-city','Frontend\HomeController@searchby_city');
Route::get('search-speciality','Frontend\HomeController@search_speciality');
Route::get('dr-detail/{id}','Frontend\HomeController@dr_detail');
Route::get('doctor-listing/{speciality}','Frontend\HomeController@dr_ls');

Route::get('see-dr/{city}','Frontend\HomeController@see_dr');

Route::get('prescription','Frontend\HomeController@prescription');

Route::get('contact','Frontend\HomeController@contact');
Route::post('send_mail','Frontend\HomeController@send_mail');

