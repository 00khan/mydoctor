@extends('frontend/master-layout')

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
    .col-sm-5{
      width: 19:8% !important;
      float: left;
      padding-left: 5px;
    }
    .image-5{
      height: 100% !important;
      width: 100% !important;
      padding: 5px !important;
    }
    .btn-conti{
      padding: 20px;
      border-radius: 80px;
      width: 263px;
      background: rgb(0,144,206);
      height: 68px;
      font-family: GothamRounded-Bold;
      font-size: 20px;

    }

    .searchbar{
    margin-bottom: auto;
    margin-top: auto;
    height: 60px;
    background-color: #fff;
    border-radius: 30px;
    padding: 10px;
    }

    .search_input{
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color:transparent;
    line-height: 40px;
    transition: width 0.4s linear;
    }

    .searchbar .search_input{
    padding: 0 10px;
    width: 100%;
    caret-color:red;
    transition: width 0.4s linear;
    color: #000;
    }

    .searchbar:hover > .search_icon{
    background: rgb(0,47,108);
    color: #fff;
    }
    .fa-star{
      color: rgb(202,203,204);
    }

    .sdr{
      font-family: GothamRounded-Bold;
    }
    .checked {
      color: rgb(0,47,108);
    }
    .active_hr{
      color: #fff;
      background: #fff;
    }
    .dr_name{
      font-family: GothamRounded-Bold;
      font-size: 19px;
    }
    .dr_name_w{
      color: #000;
      font-family: GothamRounded-Bold;
      font-size: 19px;
    }
    .dg{
      color: white;
      font-family: Gotham-Rounded-Medium;
      font-size: 16.5px;
    }
    .d_name{
      color: white;
      font-family: GothamRounded-Book;
      font-size: 14px;

    }
    .dg_b{
      color: #000;
      font-family: Gotham-Rounded-Medium;
      font-size: 16.5px;
    }
    .d_name_b{
      color: rgb(104,104,104);
      font-family: GothamRounded-Book;
      font-size: 14px;

    }
    .checked_w{
      color: rgb(89,185,75);
    }
    .fa_w{
      color: rgb(202,203,204);
    }
  </style>
@endpush

@section('content')

  <section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center" style="padding-top: 111px;">
          <div class="col-lg-12">
            <div class="banner_content text-center">
              <div class="page_link">
                <h3 style="color: #fff;">Cardiologist Doctor’s</3>
              </div>
              <div class="container h-100">
                <div class="d-flex justify-content-center h-100">
                  <div class="searchbar">
                    <input class="search_input" type="text" name="" placeholder="Search in City...">
                    <a href="#" class="search_icon">Search</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


    <!--================Contact Area =================-->
    <section class="contact_area">
      <div class="container-fluid">
        <div class="row" style="padding: 30px;">
          <div class="col-sm-12 text-center">
            <h2 class="sdr">See Doctor’s</h2>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-12 d_ab" >
            <div class="row" style="">
              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(0,144,206)">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked"></span>
                                          <span class="fa fa-star checked"></span>
                                          <span class="fa fa-star checked"></span>
                                          <span class="fa fa-star"></span>
                                          <span class="fa fa-star"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_a.png" style="float: right; margin-right: -10px;">
                                        </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: #fff; padding: 5px;"></i>
                                    <span><strong class="dg">Doctor Degree:</strong> <span class="d_name">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: #fff; padding: 10px;"></i>
                                  <span><strong class="dg">Contact No:</strong> <span class="d_name">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="" style="background-color: rgb(244,247,252);">
                  <div class="row" style="padding: 0px 20px;">
                      <div class="col-sm-12">
                          <div class="row">
                              <div class="com-sm-4">
                                  <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" style="border-radius: 100%; padding: 14px;">
                              </div>
                              <div class="col-sm-8" style="padding-top: 30px; padding-right: 0px !important">
                                  <div class="row" style="padding-right: 0px !important">
                                      <div class="col-sm-10" style="padding-right: 0px !important">
                                          <h3 class="dr_name_w" style="margin-bottom: 2px;">Dr.Danish Shahid </h3>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star checked checked_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                          <span class="fa fa-star fa_w"></span>
                                      </div>
                                      <div class="col-sm-2" style="padding-right: 0px !important; float: right;">
                                        <a href="{{url('dr-detail')}}">
                                          <img src="{{asset('public/assets')}}/img/left_s_b.png" style="float: right; margin-right: -10px;">
                                        </a>

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <hr class="active_hr">
                      </div>

                      <div class="col-sm-12" style="padding: 0px 30px;">
                          <div class="row">
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-graduation-cap fa-lg" style="color: rgb(0,144,206); padding: 5px;"></i>
                                    <span><strong class="dg_b">Doctor Degree:</strong> <span class="d_name_b">M.B.B.S</span></span> 
                                  </h5>
                              </div>
                              <div class="col-sm-12">
                                  <h5>
                                    <i class="fa fa-phone fa-lg" aria-hidden="true" style="color: rgb(0,144,206); padding: 10px;"></i>
                                  <span><strong class="dg_b">Contact No:</strong> <span class="d_name_b">0323-6667782, 0324-4061258</span></span> 
                                </h5>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>

          </div>
          </div>
              <div class="col-sm-4"></div>
              <div class="col-sm-4"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--================Contact Area =================-->




@endsection
  
@push('script')

@endpush