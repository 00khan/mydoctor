@php

$user_agent     =   $_SERVER['HTTP_USER_AGENT'];

function getOS() { 

    global $user_agent;

    $os_platform    =   "Unknown OS Platform";

    $os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

    foreach ($os_array as $regex => $value) { 

        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }

    }   

    return $os_platform;

}

function getBrowser() {

    global $user_agent;

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) { 

        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }

    }

    return $browser;

}


$user_os        =   getOS();
$user_browser   =   getBrowser();
// dd($user_browser);
if(strtolower($user_os) == "android"){
  header("Location:https://play.google.com/store/apps/details?id=com.nopso.pk.salonApp");
}
else if(strtolower($user_os) == "iphone" || strtolower($user_os) == "ipad" || strtolower($user_os) == "ipod"){
  header("Location:https://itunes.apple.com/us/app/beautyapp-pk/id1436684104?ls=1&mt=8");
}
else{
  header("Location:/index.php");
}
$device_details =   "<strong>Browser: </strong>".$user_browser."<br /><strong>Operating System: </strong>".$user_os."";

// print_r($device_details);

// echo("<br /><br /><br />".$_SERVER['HTTP_USER_AGENT']."");

@endphp


{{-- @extends('frontend/master-layout')

@push('style')

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
    .oab{
      font-family: Nexa-Bold; 
          font-size: 28px !important;
          text-align: left;
    }
    .icon_box{
      background-color: rgb(0,47,108);
      border-radius: 180px;
      width: 85px;
      height: 85px;
      padding: 24px 10px 10px 23px;
    }
    .five_div{
      margin-left: 19px;
      margin-right: 19px;
    }
    .icon_box_c{
      background-color: #fff;
      border-radius: 180px;
      width: 133px;
      height: 133px;
      padding: 30px 10px 10px 31px;
      margin: 0px auto;
      border: 10px solid #F8FAFD;
    }
    .text_al{
      text-align: center;
    }
    .single_feature_h{
      height: 460px !important;
    }
    .rounded_gold{
      font-family: GothamRounded-Bold;
      font-size: 25px !important;
    }
    .rounded_book{
      font-family: GothamRounded-Book;
      font-size: 18px;
    }
    .fin_d{
      font-family: GothamRounded-Bold;
      font-size: 50px !important;
    }
    .rounded_medium{
      font-family: Gotham-Rounded-Medium;
      font-size: 19.5px;
    }
    .s_rounded_bok{
      font-family: GothamRounded-Book;
      font-size: 15.5px;
    }
    .gotop{
      font-family: GothamRounded-Book;
      font-size: 16px;
    }
    .check_c{
      font-family: Gotham-Rounded-Medium;
      font-size: 40px;
      text-align: left;
    }
    .check_text{
      font-family: GothamRounded-Book;
      font-size: 20px;
    }
    .single-trainer .meta-text{
      box-shadow:0px !important;
    }

    /*.carousel-item{min-height:300px; background:#ccc; }*/
    

    .carousel-control-next-icon, .carousel-control-prev-icon{
      color: #000;
    }
    .carousel-control-next, .carousel-control-prev{
          top: 89% !important;
        bottom: 0 !important;
    }
  </style>
@endpush

@section('content')
	 <!--================ Start Home Banner Area =================-->
    <section class="home_banner_area">
      <div class="banner_inner">
        <div class="container-fluid" style="position: absolute; margin-top: -180px;">
          <div class="row">
            <div class="clearfix"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-12">
              <div class="row">
                <div class="col-12">
                  <div class="banner_content text-center">
                    <h2 class="text-uppercase oab" style="">

                      Fond Your Doctor <br>
                      Easy to use search to find doctor for your specific need
                    </h2>
                    <h2 class="text-uppercase mt-4 mb-5" style="font-family: Gotham-Rounded-Medium; font-size: 17px; text-align: left;">
                      Download MyDoctorApp<br>
                      Find a Doctor Online<br>
                      Book An Appointment <br><br>

                    </h2>
                  </div>
                </div>
              </div>
              <div class="col-12">
               
                    <img src="{{asset('assets')}}/img/playstore.png" style="padding-right: 10px;">
                     <img src="{{asset('assets')}}/img/appstore.png">

               </div>
              </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-12">
              <img src="{{asset('assets/img')}}/3images.png" style="width: 100% !important;">
            </div>
          </div>
        </div>
      </div>
    </section>
 

@endsection
	
@push('script')

@endpush --}}