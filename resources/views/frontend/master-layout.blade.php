<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- <link rel="icon" href="{{asset('public/assets')}}/img/favicon.png" type="image/png" /> --}}
    <title>My Doctor Pakistan </title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/bootstrap.css" />
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/flaticon.css" />
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/themify-icons.css" />
    <link rel="stylesheet" href="{{asset('public/assets')}}/vendors/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="{{asset('public/assets')}}/vendors/nice-select/css/nice-select.css" />
    <!-- main css -->
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets')}}/css/custom_style.css">
    <style type="text/css">
      
      @font-face {
        font-family: "Nexa-Bold";
        src: url("{{asset('public/assets')}}/fonts/NexaBold/Nexa_Bold.otf");
      }
      @font-face {
        font-family: "Gotham-Rounded-Medium";
        src: url("{{asset('public/assets')}}/fonts/Gotham_Rounded_Medium/Gotham_Rounded_Medium.otf");
      }
      @font-face {
        font-family: "GothamRounded-Bold";
        src: url("{{asset('public/assets')}}/fonts/Gotham_Rounded_Medium/GothamRoundedBold.ttf");
      }
       @font-face {
        font-family: "GothamRounded-Book";
        src: url("{{asset('public/assets')}}/fonts/Gotham_Rounded_Medium/Gotham_Rounded_Book.otf");
      }
      
      .section_gap {
        padding: 0px !important;
        min-height: 80px !important;
        background-color: rgb(0,144,206) !important;
      }
          
    </style>
    @stack('style')
  </head>

  <body>
    <!--================ Start Header Menu Area =================-->
    <header class="header_area">
      <div class="main_menu">
        {{-- <div class="search_input" id="search_input_box">
          <div class="container">
            <form class="d-flex justify-content-between" method="" action="">
              <input
                type="text"
                class="form-control"
                id="search_input"
                placeholder="Search Here"
              />
              <button type="submit" class="btn"></button>
              <span
                class="ti-close"
                id="close_search"
                title="Close Search"
              ></span>
            </form>
          </div>
        </div> --}}

        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container-fluid naviga">
            <a class="navbar-brand logo_h" href="{{url('/')}}"
              ><img src="{{asset('public/assets')}}/img/logo.png" alt=""
            /></a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent1"
              aria-controls="navbarSupportedContent1"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="icon-bar"></span> <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div
              class="collapse navbar-collapse offset"
              id="navbarSupportedContent1"
            >
              <ul class="nav navbar-nav menu_nav ml-auto">
                <li class="nav-item @if(Request::is('search-speciality')) active @else @endif ">
                  <a class="nav-link" href="{{url('search-speciality')}}">Find a specialist</a>
                </li>
                <li class="nav-item @if(Request::is('search-by-city')) active @else @endif">
                  <a class="nav-link" href="{{url('search-by-city')}}">Find doctor by city </a>
                </li>
                {{-- <li class="nav-item @if(Request::is('search-speciality')) active @else @endif">
                  <a class="nav-link" href="{{url('search-speciality')}}">Find doctor by name, address, phone number</a>
                </li> --}}
                <li class="nav-item @if(Request::is('d')) active @else @endif">
                  <a class="nav-link" href="{{url('d')}}">Contact online doctor</a>
                </li>
                <li class="nav-item @if(Request::is('d_')) active @else @endif">
                  <a class="nav-link" href="{{url('d')}}">Get prescription now </a>
                </li>
                <!-- <li class="nav-item">
                  <a href="{{asset('public/assets')}}/#" class="nav-link search" id="search">
                    <i class="ti-search"></i>
                  </a>
                </li> -->
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <!--================ End Header Menu Area =================-->
    @yield('content')
    
    <nav class="navbar navbar-expand-lg navbar-light" id="navbar_h_auto" style="background-color: rgb(0,47,108)">
          <div class="container-fluid">
            <a class="navbar-brand logo_h" href="{{url('/')}}"
              ><img src="{{asset('public/assets')}}/img/logo.png" alt=""
            /></a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="icon-bar"></span> <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div
              class="collapse navbar-collapse offset"
              id="navbarSupportedContent">
              <ul class="nav navbar-nav menu_nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link f_menu" href="{{url('/')}}" style="color: #fff;">Find A Specialist</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link f_menu" href="{{url('search-by-city')}}" style="color: #fff;">Find Doctor By City </a>
                </li>
               {{--  <li class="nav-item">
                  <a class="nav-link f_menu" href="{{url('search-speciality')}}" style="color: #fff;">Find Doctor By Name, Address, Phone Number</a>
                </li> --}}
                <li class="nav-item">
                  <a class="nav-link f_menu" href="{{url('d')}}" style="color: #fff;">Contact Online Doctor</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link f_menu" href="{{url('d')}}" style="color: #fff;">Get Prescription Now </a>
                </li>
                <!-- <li class="nav-item">
                  <a href="{{asset('public/assets')}}/#" class="nav-link search" id="search">
                    <i class="ti-search"></i>
                  </a>
                </li> -->
              </ul>
            </div>
          </div>
    </nav>
    <footer class="footer-area section_gap">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-8">
            <h5 class="copy_right">Project By Nopso Copyright@ {{ now()->year }}</h5>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 pull-right" style="float: right; text-align: right;">
            <h5 class="copy_right">
              <a href="" style="padding: 8px;">
                <img src="{{asset('public/assets')}}/img/facebook.png">
              </a>
              <a href="" style="padding: 0px;">
                <img src="{{asset('public/assets')}}/img/twitter.png">
              </a>
              <a href="" style="padding: 10px;">
                <img src="{{asset('public/assets')}}/img/instagram.png">
              </a>
            </h5>
          </div>
        </div>
      </div>
    </footer>
    <!--================ End footer Area  =================-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('public/assets')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{asset('public/assets')}}/js/popper.js"></script>
    <script src="{{asset('public/assets')}}/js/bootstrap.min.js"></script>
    <script src="{{asset('public/assets')}}/vendors/nice-select/js/jquery.nice-select.min.js"></script>
    <script src="{{asset('public/assets')}}/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="{{asset('public/assets')}}/js/owl-carousel-thumb.min.js"></script>
    <script src="{{asset('public/assets')}}/js/jquery.ajaxchimp.min.js"></script>
    <script src="{{asset('public/assets')}}/js/mail-script.js"></script>
    <!--gmaps Js-->
    {{-- <script src="{{asset('public/assets')}}/https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script> --}}
    <script src="{{asset('public/assets')}}/js/gmaps.min.js"></script>
    <script src="{{asset('public/assets')}}/js/theme.js"></script>


    <script type="text/javascript">
          $('#sidebar-link').click(function(event) {
          var sidebar = $(this).siblings('ul').hasClass('show');
          if (!sidebar) {
            $('.sidebar-dropdown').removeClass('show');
          }
        });

    </script>
    @stack('script')
  </body>
</html>
