@extends('frontend/master-layout')

@push('style')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
    
<link rel='stylesheet' href='http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>

  <style type="text/css">
    .col-sm-5{
      width: 19:8% !important;
      float: left;
      padding-left: 5px;
    }
    .image-5{
      height: 100% !important;
      width: 100% !important;
      padding: 5px !important;
    }
    .btn-conti{
      padding: 20px;
      border-radius: 80px;
      width: 263px;
      background: rgb(0,144,206);
      height: 68px;
      font-family: GothamRounded-Bold;
      font-size: 20px;

    }



    .searchbar{
    margin-bottom: auto;
    margin-top: auto;
    height: 60px;
    /*background-color: #fff;*/
    border-radius: 30px;
    /*padding: 10px;*/
    }

    .search_input{
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color:transparent;
    line-height: 40px;
    transition: width 0.4s linear;
    }

    .searchbar .search_input{
    padding: 0 10px;
    width: 450px;
    caret-color:red;
    transition: width 0.4s linear;
    }

    .searchbar:hover > .search_icon{
    background: #fff;
    color: rgb(0,144,206);
    }

    .search_icon{
      margin-top: -3px;
      height: 68px;
      width: 80%;
      float: right;
      display: flex;
      justify-content: center;
      border-radius: 27.5px;
      color: white;
      background-color: #fff;
      font-size: 20px;
      font-family: GothamRounded-Bold;
      padding-top: 24px;
      margin-right: 0px;
      color: rgb(0,144,206);
      margin-right: 40px !important;

    }
    .gouth_med{
      font-family: Gotham-Rounded-Medium;
      font-size: 16px;
      padding-top: 25px;
    }
    .d_ab{
      background-color: rgb(244,247,252);
    }

.avail{
  font-family: GothamRounded-Book;
  font-size: 19px;
  color: rgb(89,185,75);
}
.d_name{
    font-family: GothamRounded-Bold;
  font-size: 25.5px;
}
.m-a-d{
  font-family: GothamRounded-Bold;
  font-size: 25px;
}

.m-a-d-txt{
  font-family: GothamRounded-Book;
  font-size: 18px;
  text-align: left;

}
.dg{
  font-family: GothamRounded-Bold;
  font-size: 25px;
}
  

  /* windows chat style code start here */
/*
    body,html{
      height: 100%;
      margin: 0;
      background: #7F7FD5;
         background: -webkit-linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);
          background: linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);
    }*/

    .chat{
      margin-top: auto;
      margin-bottom: auto;
    }
    .card{
      height: 500px;
      border-radius: 15px !important;
      background-color: rgba(0,0,0,0.4) !important;
    }
    .contacts_body{
      padding:  0.75rem 0 !important;
      overflow-y: auto;
      white-space: nowrap;
    }
    .msg_card_body{
      overflow-y: auto;
    }
    .card-header{
      border-radius: 15px 15px 0 0 !important;
      border-bottom: 0 !important;
    }
   .card-footer{
    border-radius: 0 0 15px 15px !important;
      border-top: 0 !important;
  }
    .container{
      align-content: center;
    }
    .search{
      border-radius: 15px 0 0 15px !important;
      background-color: rgba(0,0,0,0.3) !important;
      border:0 !important;
      color:white !important;
    }
    .search:focus{
         box-shadow:none !important;
           outline:0px !important;
    }
    .type_msg{
      background-color: rgba(0,0,0,0.3) !important;
      border:0 !important;
      color:white !important;
      height: 60px !important;
      overflow-y: auto;
    }
      .type_msg:focus{
         box-shadow:none !important;
           outline:0px !important;
    }
    .attach_btn{
  border-radius: 15px 0 0 15px !important;
  background-color: rgba(0,0,0,0.3) !important;
      border:0 !important;
      color: white !important;
      cursor: pointer;
    }
    .send_btn{
  border-radius: 0 15px 15px 0 !important;
  background-color: rgba(0,0,0,0.3) !important;
      border:0 !important;
      color: white !important;
      cursor: pointer;
    }
    .search_btn{
      border-radius: 0 15px 15px 0 !important;
      background-color: rgba(0,0,0,0.3) !important;
      border:0 !important;
      color: white !important;
      cursor: pointer;
    }
    .contacts{
      list-style: none;
      padding: 0;
    }
    .contacts li{
      width: 100% !important;
      padding: 5px 10px;
      margin-bottom: 15px !important;
    }
  /*.active{
      background-color: rgba(0,0,0,0.3);
  }*/
    .user_img{
      height: 70px;
      width: 70px;
      border:1.5px solid #f5f6fa;
    
    }
    .user_img_msg{
      height: 90px;
      width: 90px;
      border:1.5px solid #f5f6fa;
    
    }
  .img_cont{
      position: relative;
      height: 70px;
      width: 70px;
  }
  .img_cont_msg{
      height: 40px;
      width: 40px;
  }
  .online_icon{
    position: absolute;
    height: 15px;
    width:15px;
    background-color: #4cd137;
    border-radius: 50%;
    bottom: 0.2em;
    right: 0.4em;
    border:1.5px solid white;
  }
  .offline{
    background-color: #c23616 !important;
  }
  .user_info{
    margin-top: auto;
    margin-bottom: auto;
    margin-left: 15px;
  }
  .user_info span{
    font-size: 20px;
    color: white;
  }
  .user_info p{
  font-size: 10px;
  color: rgba(255,255,255,0.6);
  }
  .video_cam{
    margin-left: 50px;
    margin-top: 5px;
  }
  .video_cam span{
    color: white;
    font-size: 20px;
    cursor: pointer;
    margin-right: 20px;
  }
  .msg_cotainer{
    margin-top: auto;
    margin-bottom: auto;
    margin-left: 10px;
    /*border-radius: 25px;*/
    background-color: #82ccdd;
    padding: 10px;
    position: relative;
  }
  .msg_cotainer_send{
    margin-top: auto;
    margin-bottom: auto;
    margin-right: 10px;
    /*border-radius: 25px;*/
    background-color: #78e08f;
    padding: 10px;
    position: relative;
  }
  .msg_time{
    position: absolute;
    left: 0;
    bottom: -15px;
    color: rgba(255,255,255,0.5);
    font-size: 10px;
  }
  .msg_time_send{
    position: absolute;
    right:0;
    bottom: -15px;
    color: rgba(255,255,255,0.5);
    font-size: 10px;
  }
  .msg_head{
    position: relative;
  }
  #action_menu_btn{
    position: absolute;
    right: 10px;
    top: 10px;
    color: white;
    cursor: pointer;
    font-size: 20px;
  }
  .action_menu{
    z-index: 1;
    position: absolute;
    padding: 15px 0;
    background-color: rgba(0,0,0,0.5);
    color: white;
    border-radius: 15px;
    top: 30px;
    right: 15px;
    display: none;
  }
  .action_menu ul{
    list-style: none;
    padding: 0;
  margin: 0;
  }
  .action_menu ul li{
    width: 100%;
    padding: 10px 15px;
    margin-bottom: 5px;
  }
  .action_menu ul li i{
    padding-right: 10px;
  
  }
  .action_menu ul li:hover{
    cursor: pointer;
    background-color: rgba(0,0,0,0.2);
  }
  @media(max-width: 576px){
  .contacts_card{
    margin-bottom: 15px !important;
  }
  }






  /*triangle left side start style codes */
  .left-arrow{
  display: inline-block;
  position: relative;
  background: pink;
  padding: 15px;
  padding-left:30px;
  margin-left:60px;
  /*border-radius: 25px;*/
}
.left-arrow:after{
  content: '';
  display: block;  
  position: absolute;
  right: 100%;
  top: 50%;
  margin-top: -30px;
  width: 0;
  height: 0;
  border-top: 30px solid transparent;
  border-right: 30px solid pink;
  border-bottom: 30px solid transparent;
  border-left: 30px solid transparent;
}



 .right_triangle{
    display: inline-block;
    position: relative;
    background: pink;
    padding: 15px;
    padding-left:30px;
    margin-left:60px;
  }
.right_triangle:after{
    display: block;
    right: 100%;
    margin-top: -30px;
    border-right: 30px solid pink;
    border-bottom: 30px solid transparent;
    content: "";
    position: absolute;
    height: 0;
    width: 0;
    left: 100%;
    top: 50%;
    border-top: 30px solid transparent;
    border: 30px solid transparent;
    border-left: 30px solid pink;
  }

  .type_msg{
    padding: 10px;
  }


  </style>
@endpush

@section('content')

  <section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center" style="padding-top: 111px;">
          <div class="col-lg-12">
            <div class="banner_content text-center">
              <div class="page_link">
                <h3 style="color: #fff; font-family: Nexa-Bold; font-size: 50px;">Dactor Danish Shahid </3>
              </div>
              <div class="container h-100">
                <div class="d-flex justify-content-center h-100">
                  <div class="searchbar">
                    <a href="#" class="search_icon">Book An Appointment</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

 {{--  <div class="left-arrow">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
    
  </div> --}}



    <!--================Contact Area =================-->
    <section class="contact_area">
      <div class="container-fluid">
        <div class="row" style="padding: 30px;">
          <div class="col-sm-12 text-center">
            <h3>
              Chat with Dr.danial shahid
            </h3>
          </div>
          <div class="col-sm-12">
              <div class="container-fluid h-100">
                <div class="row justify-content-center h-100">
                  <div class="col-md-12 col-xl-12 chat">
                    <div class="card">

                      <div class="card-body msg_card_body">
                        <div class="d-flex justify-content-start mb-4">
                          <div class="col-1" >
                            <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                              <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                            </div>
                          </div>
                          <div class="col-11" style="margin-left: -60px !important;">
                            <div class="left-arrow" style="min-height: 100px; min-width: 100%;">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                              
                            </div>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                            <div class="col-11">
                              <div class="msg_cotainer_send right_triangle" style="float: right;">
                                <div class="" style="min-height: 100px; min-width: 100%;">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-1">
                              <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                                <img src="{{asset('public/assets')}}/img/dr2.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                              </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-start mb-4">
                          <div class="col-1" >
                            <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                              <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                            </div>
                          </div>
                          <div class="col-11" style="margin-left: -60px !important;">
                            <div class="left-arrow" style="min-height: 100px; min-width: 100%;">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                              
                            </div>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                            <div class="col-11">
                              <div class="msg_cotainer_send right_triangle" style="float: right;">
                                <div class="" style="min-height: 100px; min-width: 100%;">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-1">
                              <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                                <img src="{{asset('public/assets')}}/img/dr2.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                              </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-start mb-4">
                          <div class="col-1" >
                            <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                              <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                            </div>
                          </div>
                          <div class="col-11" style="margin-left: -60px !important;">
                            <div class="left-arrow" style="min-height: 100px; min-width: 100%;">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                              
                            </div>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                            <div class="col-11">
                              <div class="msg_cotainer_send right_triangle" style="float: right;">
                                <div class="" style="min-height: 100px; min-width: 100%;">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-1">
                              <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                                <img src="{{asset('public/assets')}}/img/dr2.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                              </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-start mb-4">
                          <div class="col-1" >
                            <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                              <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                            </div>
                          </div>
                          <div class="col-11" style="margin-left: -60px !important;">
                            <div class="left-arrow" style="min-height: 100px; min-width: 100%;">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                              
                            </div>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                            <div class="col-11">
                              <div class="msg_cotainer_send right_triangle" style="float: right;">
                                <div class="" style="min-height: 100px; min-width: 100%;">
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea reiciendis facilis soluta a ut doloribus architecto provident, labore minus sint doloremque in, ipsum magnam! Optio amet asperiores, sed recusandae reiciendis.</p>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quis est aliquid dicta a illo, officiis, doloribus odit perspiciatis? Laboriosam incidunt dolor harum ipsum, molestiae ratione molestias blanditiis qui beatae.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-1">
                              <div class="img_cont_msg" style="display: block; width: 100% !important; height: 100%; position: relative;">
                                <img src="{{asset('public/assets')}}/img/dr2.jpg" class="rounded-circle user_img_msg" style="position: relative; top: 50%; left: 50%; transform: translate(-50%,-50%);">
                              </div>
                            </div>
                        </div>

                       {{--  <div class="d-flex justify-content-start mb-4">
                          <div class="img_cont_msg">
                            <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg">
                          </div>
                          <div class="msg_cotainer">
                            I am good too, thank you for your chat template
                            <span class="msg_time">9:00 AM, Today</span>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                          <div class="msg_cotainer_send">
                            You welcome Maryam
                            <span class="msg_time_send">9:05 AM, Today</span>
                          </div>
                          <div class="img_cont_msg">
                        <img src="{{asset('public/assets')}}/img/dr2.jpg"" class="rounded-circle user_img_msg">
                          </div>
                        </div>
                        <div class="d-flex justify-content-start mb-4">
                          <div class="img_cont_msg">
                            <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg">
                          </div>
                          <div class="msg_cotainer">
                            I am looking for your next templates
                            <span class="msg_time">9:07 AM, Today</span>
                          </div>
                        </div>
                        <div class="d-flex justify-content-end mb-4">
                          <div class="msg_cotainer_send">
                            Ok, thank you have a good day
                            <span class="msg_time_send">9:10 AM, Today</span>
                          </div>
                          <div class="img_cont_msg">
                      <img src="{{asset('public/assets')}}/img/dr2.jpg" class="rounded-circle user_img_msg">
                          </div>
                        </div>
                        <div class="d-flex justify-content-start mb-4">
                          <div class="img_cont_msg">
                            <img src="{{asset('public/assets')}}/img/dr1.jpg" class="rounded-circle user_img_msg">
                          </div>
                          <div class="msg_cotainer">
                            Bye, see you
                            <span class="msg_time">9:12 AM, Today</span>
                          </div>
                        </div> --}}
                      </div>
                      <div class="card-footer">
                        <div class="input-group">
                          <div class="input-group-append">
                            {{-- <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span> --}}
                          </div>
                          <textarea name="" class="form-control type_msg" placeholder="Write your message here..."></textarea>
                          <div class="input-group-append">
                            <img src="{{asset('public/assets')}}/img/upload.png">
                            <img src="{{asset('public/assets')}}/img/attachment.png">
                            <span style="    background-color: rgb(0,47,108);
    border-radius: 100%;
    width: 60px;
    height: 60px !important;">
                            <img src="{{asset('public/assets')}}/img/send.png" style="    margin-top: 12px;
    margin-left: 10px;">
                          </span>
                            {{-- <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span> --}}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <!--================Contact Area =================-->




@endsection
  
@push('script')
  <script type="text/javascript">
      $(document).ready(function(){
      $('#action_menu_btn').click(function(){
        $('.action_menu').toggle();
      });
        });
  </script>

@endpush  