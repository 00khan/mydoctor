@extends('frontend/master-layout')

@push('style')
<link rel='stylesheet' href='http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>

  <style type="text/css">
    .col-sm-5{
      width: 19:8% !important;
      float: left;
      padding-left: 5px;
    }
    .image-5{
      height: 100% !important;
      width: 100% !important;
      padding: 5px !important;
    }
    .btn-conti{
      padding: 20px;
      border-radius: 80px;
      width: 263px;
      background: rgb(0,144,206);
      height: 68px;
      font-family: GothamRounded-Bold;
      font-size: 20px;

    }



    .searchbar{
    margin-bottom: auto;
    margin-top: auto;
    height: 60px;
    /*background-color: #fff;*/
    border-radius: 30px;
    /*padding: 10px;*/
    }

    .search_input{
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color:transparent;
    line-height: 40px;
    transition: width 0.4s linear;
    }

    .searchbar .search_input{
    padding: 0 10px;
    width: 450px;
    caret-color:red;
    transition: width 0.4s linear;
    }

    .searchbar:hover > .search_icon{
    background: #fff;
    color: rgb(0,144,206);
    }

    .search_icon{
      margin-top: -3px;
      height: 68px;
      width: 80%;
      float: right;
      display: flex;
      justify-content: center;
      border-radius: 27.5px;
      color: white;
      background-color: #fff;
      font-size: 20px;
      font-family: GothamRounded-Bold;
      padding-top: 24px;
      margin-right: 0px;
      color: rgb(0,144,206);
      margin-right: 40px !important;

    }
    .gouth_med{
      font-family: Gotham-Rounded-Medium;
      font-size: 16px;
      padding-top: 25px;
    }
    .d_ab{
      background-color: rgb(244,247,252);
    }

    /*style code for star rating start here */
    .rating_stars {
        margin-top: 0px;
        display: inline-block;
        font-size: 20px;
        font-weight: 200;
        color: #918f8f;
        position: relative;
    }

.rating_stars span .fa, .rating_stars span.active-low .fa-star-o, .rating_stars span.active-high .fa-star-o{
  display: none;
}

.rating_stars span .fa-star-o{
  display: inline-block;
}

.rating_stars span.s.active-high .fa-star{
  display: inline-block; color: rgb(89,185,75);
}

.rating_stars span.s.active-low .fa-star-half-o{
  display: inline-block; color: rgb(89,185,75);
}

.rating_stars span.r {
  position: absolute;
  top: 0;
  height: 20px;
  width: 10px;
  left: 0;
}

.rating_stars span.r.r0_5 {left:0px;}
.rating_stars span.r.r1 {left:10px; width: 11px;}
.rating_stars span.r.r1_5 {left:21px; width: 13px;}
.rating_stars span.r.r2 {left:34px; width: 12px;}
.rating_stars span.r.r2_5 {left:46px; width: 12px;}
.rating_stars span.r.r3 {left:58px; width: 11px;}
.rating_stars span.r.r3_5 {left:69px; width: 12px;}
.rating_stars span.r.r4 {left:81px; width: 12px;}
.rating_stars span.r.r4_5 {left:93px; width: 12px;}
.rating_stars span.r.r5 {left:105px; width: 12px;}

/* Just to make things look pretty ;) */

label {width: 100px;display: inline-block; text-align: right; margin-right: 10px;}
input {width: 50px; text-align: center;}
.values {margin-top: 20px;}
.info {max-width: 500px; margin: 20px auto;}
/*end star rating style code */

.avail{
  font-family: GothamRounded-Book;
  font-size: 19px;
  color: rgb(89,185,75);
}
.d_name{
    font-family: GothamRounded-Bold;
  font-size: 25.5px;
}
.m-a-d{
  font-family: GothamRounded-Bold;
  font-size: 25px;
}

.m-a-d-txt{
  font-family: GothamRounded-Book;
  font-size: 18px;
  text-align: left;

}
.dg{
  font-family: GothamRounded-Bold;
  font-size: 25px;
}



 .fa-star{
      color: rgb(202,203,204);
    }

    .sdr{
      font-family: GothamRounded-Bold;
    }
    .checked {
      color: rgb(0,47,108);
    }
    .active_hr{
      color: #fff;
      background: #fff;
    }
    .dr_name{
      font-family: GothamRounded-Bold;
      font-size: 19px;
    }
    .dr_name_w{
      color: #000;
      font-family: GothamRounded-Bold;
      font-size: 19px;
    }
    /*.dg{
      color: white;
      font-family: Gotham-Rounded-Medium;
      font-size: 16.5px;
    }*/
    .d_name{
      color: white;
      font-family: GothamRounded-Book;
      font-size: 14px;

    }
    .dg_b{
      color: #000;
      font-family: Gotham-Rounded-Medium;
      font-size: 16.5px;
    }
    .d_name_b{
      color: rgb(104,104,104);
      font-family: GothamRounded-Book;
      font-size: 14px;

    }
    .checked_w{
      color: rgb(89,185,75);
    }
    .fa_w{
      color: rgb(202,203,204);
    }


  </style>
@endpush

@section('content')

  <section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center" style="padding-top: 111px;">
          <div class="col-lg-12">
            <div class="banner_content text-center">
              <div class="page_link">
                <h3 style="color: #fff; font-family: Nexa-Bold; font-size: 50px;">
                  {{$dd->sal_name}}
                  {{-- Dactor Danish Shahid  --}}
                  
                </3>
              </div>
              <div class="container h-100">
                <div class="d-flex justify-content-center h-100">
                  <div class="searchbar">
                    <a href="{{url('d')}}" target="blank" class="search_icon">Book An Appointment</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


    <!--================Contact Area =================-->
    <section class="contact_area">
      <div class="container-fluid">
        <div class="row" style="padding: 30px;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12 text-center" style="padding: 30px;">
            <h2>Doctor Detail</h2>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12 d_ab" >
            <div class="row" style="padding: 40px;">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12 ">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 ">
                     @if($dd->sal_pic != '')
                        <img src="https://mydr.qwpcorp.com/apis_pk/salonimages/{{$dd->sal_pic}}" width="100px" height="100px" style="border-radius: 100%;">
                        @else
                          <img src="{{asset('public/assets')}}/img/dr_detail.jpg" width="100px" height="100px" style="border-radius: 100%;">
                        @endif

                    
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-8 ">
                    <h4 class="avail">
                      Available Now
                    </h4>
                    <h3>
                        {{$dd->sal_name}}

                        <img src="{{asset('public/assets')}}/img/chcked.png"></h3>
                    <span class="rating_stars rating_0">
                      
                                              @if($dd->sal_rating > 0 && $dd->sal_rating <= 1)
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              @elseif($dd->sal_rating > 1 && $dd->sal_rating <= 2)
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              @elseif($dd->sal_rating > 2 && $dd->sal_rating <= 3)
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              @elseif($dd->sal_rating > 3 && $dd->sal_rating <= 4)
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              @elseif($dd->sal_rating > 4 && $dd->sal_rating <= 5)
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              <span class="fa fa-star checked checked_w "></span>
                                              @else 
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              <span class="fa fa-star fa_w "></span>
                                              @endif
                                            
                                        

                      {{-- <span class='s' data-low='0.5' data-high='1'><i class="fa fa-star-o"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star"></i></span>
                      <span class='s' data-low='1.5' data-high='2'><i class="fa fa-star-o"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star"></i></span>
                      <span class='s' data-low='2.5' data-high='3'><i class="fa fa-star-o"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star"></i></span>
                      <span class='s' data-low='3.5' data-high='4'><i class="fa fa-star-o"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star"></i></span>
                      <span class='s' data-low='4.5' data-high='5'><i class="fa fa-star-o"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star"></i></span>
                                
                      <span class='r r0_5' data-rating='1' data-value='0.5'></span>
                      <span class='r r1' data-rating='1' data-value='1'></span>
                      <span class='r r1_5' data-rating='15' data-value='1.5'></span>
                      <span class='r r2' data-rating='2' data-value='2'></span>
                      <span class='r r2_5' data-rating='25' data-value='2.5'></span>
                      <span class='r r3' data-rating='3' data-value='3'></span>
                      <span class='r r3_5' data-rating='35' data-value='3.5'></span>
                      <span class='r r4' data-rating='4' data-value='4'></span>
                      <span class='r r4_5' data-rating='45' data-value='4.5'></span>
                      <span class='r r5' data-rating='5' data-value='5'></span> --}}
                    </span>
                  </div>
                      <input type="hidden" name="rating" id="rating_val" value="0" />
                  <div class="col-sm-12 d_d_n">
                    <h5>
                      <span>
                        <strong class="dg">Doctor Degree: </strong> 
                        <span>
                          ({{$dd->sal_degree}})
                        </span>
                      </span> 
                    </h5>
                  </div>
                </div>
              </div>
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-12">
                <div class="row">
                  <div class="col-sm-12">
                    <h2 class="m-a-d">More About Doctor</h2>
                    <h5 class="m-a-d-txt">
                      @if($dd->sal_biography != '')
                      {{$dd->sal_biography}}
                      @else
                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce in rutrum orci, vulputate venenatis nibh. Suspendisse a ipsum non. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin id blandit risus. Curabitur vestibulum justo augue, sit amet condimentum eros accumsan et.
                      @endif
                    </h5>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12 " style="padding-top: 40px;">
                    <div class="row">
                      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-12">
                        <h5><strong class="dg">Contact No: </strong>
                          {{$dd->sal_phone}}
                          {{-- 0324-4016258, 0323-6667728 --}}
                          
                        </h5>
                      </div>
                      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-12">
                        <strong>Timings:</strong> (8am To 9pm)
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12 text-center">
                <h2 class="pr">Patient Review’s</h2>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12" style="padding: 0px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12" style="padding: 0px; padding-top: 50px;">
                   <div class="container-fluid" style="padding: 0px;">
                    <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12" style="padding: 0px;">
                      <div id="carousel2_indicator" class="carousel slide carousel-fade" data-ride="carousel">
                          <div class="carousel-inner">
                              <div class="carousel-item active">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="row">
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <div style="margin-top: 30px; background-color: rgb(0,144,206); width: 273px; height: 270px !important; margin-right: 50px;">
                                                        <img src="{{asset('public/assets')}}/img/dr.png" style="width: 273px; height: 286px; margin-left: 15px; margin-top: -30px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <h2 class="check_c">Check What our client say about us 1</h2>
                                                      <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                                      <h5 style="text-align: left;">Zeeshan Designer</h5>
                                                      <h5 style="text-align: left;">Patient</h5>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-6">
                                              <div class="row">
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <div style="margin-top: 30px; background-color: rgb(0,144,206); width: 273px; height: 270px !important; margin-right: 50px;">
                                                        <img src="{{asset('public/assets')}}/img/dr1.jpg" style="width: 273px; height: 286px; margin-left: 15px; margin-top: -30px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <h2 class="check_c">Check What our client say about us 2</h2>
                                                      <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                                      <h5 style="text-align: left;">Zeeshan Designer</h5>
                                                      <h5 style="text-align: left;">Patient</h5>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <div class="col-sm-12">
                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="row">
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <div style="margin-top: 30px; background-color: rgb(0,144,206); width: 273px; height: 270px !important; margin-right: 50px;">
                                                        <img src="{{asset('public/assets')}}/img/dr2.jpg" style="width: 273px; height: 286px; margin-left: 15px; margin-top: -30px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <h2 class="check_c">Check What our client say about us 3</h2>
                                                      <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                                      <h5 style="text-align: left;">Zeeshan Designer</h5>
                                                      <h5 style="text-align: left;">Patient</h5>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-6">
                                              <div class="row">
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <div style="margin-top: 30px; background-color: rgb(0,144,206); width: 273px; height: 270px !important; margin-right: 50px;">
                                                        <img src="{{asset('public/assets')}}/img/dr.png" style="width: 273px; height: 286px; margin-left: 15px; margin-top: -30px;">
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-6 col-md-12 col-12">
                                                      <h2 class="check_c">Check What our client say about us 3</h2>
                                                      <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                                      <h5 style="text-align: left;">Zeeshan Designer</h5>
                                                      <h5 style="text-align: left;">Patient</h5>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-sm-12 text-center">
                                <a class="carousel-control-prev next_s" href="#carousel2_indicator" role="button" data-slide="prev" style="">
                                    <img src="{{asset('public/assets')}}/img/left_arrow.png">
                                    <span class="sr-only">Previous</span>

                                </a>
                                <a class="carousel-control-next previous_s" href="#carousel2_indicator" role="button" data-slide="next" style="">
                                    <img src="{{asset('public/assets')}}/img/right_arrow.png">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                          </div>
                          <br><br><br>
                        <div class="clearfix"></div>
                      </div>
                  </aside>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-sm-12 text-center" style="clear: both; padding: 50px 0px;">
            <a href="{{url('d')}}" target="blank" class="btn btn-primary btn-conti" style="width: 409px; height: 68px;">Book An Appointment</a>
          </div>
        </div>
      </div>
    </section>
    <!--================Contact Area =================-->




@endsection
  
@push('script')
  <script type="text/javascript">
    jQuery(document).ready(function($) {
$('.rating_stars span.r').hover(function() {
            // get hovered value
            var rating = $(this).data('rating');
            var value = $(this).data('value');
            $(this).parent().attr('class', '').addClass('rating_stars').addClass('rating_'+rating);
            highlight_star(value);
        }, function() {
            // get hidden field value
            var rating = $("#rating").val();
            var value = $("#rating_val").val();
            $(this).parent().attr('class', '').addClass('rating_stars').addClass('rating_'+rating);
            highlight_star(value);
        }).click(function() {
            // Set hidden field value
            var value = $(this).data('value');
            $("#rating_val").val(value);

            var rating = $(this).data('rating');
            $("#rating").val(rating);
            
            highlight_star(value);
        });
        
        var highlight_star = function(rating) {
            $('.rating_stars span.s').each(function() {
                var low = $(this).data('low');
                var high = $(this).data('high');
                $(this).removeClass('active-high').removeClass('active-low');
                if (rating >= high) $(this).addClass('active-high');
                else if (rating == low) $(this).addClass('active-low');
            });
        }
});
  </script>
@endpush  