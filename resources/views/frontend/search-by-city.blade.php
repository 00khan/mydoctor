@extends('frontend/master-layout')

@push('style')
  <style type="text/css">
    .col-sm-5{
      width: 19:8% !important;
      float: left;
      padding-left: 5px;
    }
    .image-5{
      height: 100% !important;
      width: 100% !important;
      padding: 5px !important;
    }
    .btn-conti{
      padding: 20px;
      border-radius: 80px;
      width: 263px;
      background: rgb(0,144,206);
      height: 68px;
      font-family: GothamRounded-Bold;
      font-size: 20px;

    }



    .searchbar{
    margin-bottom: auto;
    margin-top: auto;
    height: 60px;
    background-color: #fff;
    border-radius: 30px;
    padding: 10px;
    }

    .search_input{
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color:transparent;
    line-height: 40px;
    transition: width 0.4s linear;
    }

    .searchbar .search_input{
    padding: 0 10px;
    width: 100%;
    caret-color:red;
    transition: width 0.4s linear;
    color: #000;
    }

    .searchbar:hover > .search_icon{
    background: rgb(0,47,108);
    color: #fff;
    }

    .search_icon{
      

    }
    /*.searchbar .search_input*/
  </style>
@endpush

@section('content')

  <section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center" style="padding-top: 111px;">
          <div class="col-lg-12">
            <div class="banner_content text-center">
              <div class="page_link">
                {{-- <h3 style="color: #fff;">Search By City</3> --}}
              </div>
              <form method="get" action="{{url('search-by-city')}}">
              <div class="container h-100">
                <div class="d-flex justify-content-center h-100">
                  <div class="searchbar">
                    <input class="search_input" name="city_name" id="city_name" type="text" placeholder="Search ..." @isset($city_name) value="{{$city_name}}" @endisset>
                    <input type="submit" class="search_icon" value="Search" style="padding-top: 4px !important; cursor: pointer !important; clear: both; position: relative;">
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


    <!--================Contact Area =================-->
    <section class="contact_area">
      <div class="container">
        <div class="row" style="padding: 30px;">
          <div class="col-12 text-center" style="padding: 30px;">
            <h2>Choose City</h2>
          </div>
          {{-- <div class="col-lg-1 colspan"></div> --}}
          @foreach($dr as $key => $value)
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
            <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
              <a href="{{url('see-dr'.'/'.$value->sal_city)}}">

                <h2 style="text-transform: uppercase;">
                  {{$value->sal_city}}
                  {{-- LHR --}}
                </h2>
                <h5>
                  {{-- {{$value->sal_city}} --}}
                  {{-- LAHORE --}}
                  
                </h5>
              </a>
            </div>
          </div>
          @endforeach
          {{-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>KHI</h2>
                  <h5>kARACHI</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>ISB</h2>
                  <h5>ISLAMABAD</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>QTA</h2>
                  <h5>QUETTA</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>PEW</h2>
                  <h5>PESHAWAR</h5>
                </a>
              </div>
          </div>
   
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
            <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
              <a href="{{url('d')}}">
                <h2>FSD</h2>
                <h5>FAISLABAD</h5>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>AAW</h2>
                  <h5>ABBOTTABAD</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>ATG</h2>
                  <h5>ATTOCK</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>WGB</h2>
                  <h5>BAHAWALNAGAR</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-6">
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2>BHV</h2>
                  <h5>BAHAWALPUR</h5>
                </a>
              </div>
          </div> --}}
          {{-- <div class="col-lg-1 colspan"></div> --}}

          <div class="col-sm-12 text-center" style="padding: 50px 0px;">
            <a href="" class="btn btn-primary btn-conti">Continue</a>
          </div>
        </div>
      </div>
    </section>
    <!--================Contact Area =================-->




@endsection
  
@push('script')

@endpush