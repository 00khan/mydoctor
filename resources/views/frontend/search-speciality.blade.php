@extends('frontend/master-layout')

@push('style')
  <style type="text/css">
    .col-sm-5{
      width: 19:8% !important;
      float: left;
      padding-left: 5px;
    }
    .image-5{
      height: 100% !important;
      width: 100% !important;
      padding: 5px !important;
    }
    .btn-conti{
      padding: 20px;
      border-radius: 80px;
      width: 263px;
      background: rgb(0,144,206);
      height: 68px;
      font-family: GothamRounded-Bold;
      font-size: 20px;

    }



    .searchbar{
    margin-bottom: auto;
    margin-top: auto;
    height: 60px;
    background-color: #fff;
    border-radius: 30px;
    padding: 10px;
    }

    .search_input{
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color:transparent;
    line-height: 40px;
    transition: width 0.4s linear;
    }

    .searchbar .search_input{
    padding: 0 10px;
    width: 100%;
    caret-color:red;
    transition: width 0.4s linear;
    color: #000;
    }

    .searchbar:hover > .search_icon{
    background: rgb(0,47,108);
    color: #fff;
    }

    .search_icon{
      

    }
    .gouth_med{
      font-family: Gotham-Rounded-Medium;
      font-size: 16px;
      padding-top: 25px;
    }
  </style>
@endpush

@section('content')

  <section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center" style="padding-top: 111px;">
          <div class="col-lg-12">
            <div class="banner_content text-center">
              <div class="page_link">
                <h3 style="color: #fff;">Search Speciality</3>
              </div>
              <div class="container h-100">
                <div class="d-flex justify-content-center h-100">
                  <div class="searchbar">
                    <input class="search_input" type="text" name="" placeholder="Search in City...">
                    <a href="#" class="search_icon">Search</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


    <!--================Contact Area =================-->
    <section class="contact_area">
      <div class="container">
        <div class="row" style="padding: 30px;">
          <div class="col-sm-12 text-center" style="padding: 30px;">
            <h2>Choose Speciality</h2>
          </div>
          @foreach($sp as $key => $value)
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-12">
            <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
              <a href="{{url('doctor-listing'.'/'.$value->sp_name)}}">
                <h2>
                  @if($value->sp_icon !='')
                  <img src="https://mydr.qwpcorp.com/apis_pk/salonimages/{{$value->sp_icon}}" width="99px" height="81px">
                  @else
                  <img src="{{asset('public/assets')}}/img/teeth.png" width="99px" height="81px">
                  @endif
                </h2>
                <h5 class="gouth_med" style="text-transform: uppercase;">
                  {{$value->sp_name}}
                  {{-- DENTITS --}}
                  
                </h5>
              </a>
            </div>
          </div>
          @endforeach
          {{-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-12">public
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('doctor-listing'.'/'.'.$value->sp_name')}}">
                  <h2>
                    <img src="{{asset('/assets')}}/img/heart.png">
                  </h2>
                  <h5 class="gouth_med" style="text-transform: uppercase;">cardiologist</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-12">public
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2><img src="{{asset('/assets')}}/img/mind.png"></h2>
                  <h5 class="gouth_med" style="text-transform: uppercase;">Neurologist</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-12">public
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2><img src="{{asset('/assets')}}/img/org.png"></h2>
                  <h5 class="gouth_med" style="text-transform: uppercase;">urologist</h5>
                </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-12">public
              <div class="" style="margin-bottom: 20px; background-color: rgb(244,247,252); text-align: center; padding: 20px;">
                <a href="{{url('d')}}">
                  <h2><img src="{{asset('/assets')}}/img/eye.png"></h2>
                  <h5 class="gouth_med" style="text-transform: uppercase;">eye specialist</h5>
                </a>
              </div>
          </div> --}}
          {{-- <div class="col-lg-1 colspan"></div> --}}

          <div class="col-sm-12 text-center" style="padding: 50px 0px;">
            <a href="" class="btn btn-primary btn-conti">Continue</a>
          </div>
        </div>
      </div>
    </section>
    <!--================Contact Area =================-->




@endsection
  
@push('script')

@endpush