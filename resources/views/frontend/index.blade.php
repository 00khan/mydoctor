@extends('frontend/master-layout')

@push('style')

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
    .oab{
      font-family: Nexa-Bold; 
          font-size: 28px !important;
          text-align: left;
    }
    .icon_box{
      background-color: rgb(0,47,108);
      border-radius: 180px;
      width: 85px;
      height: 85px;
      padding: 24px 10px 10px 23px;
    }
    .five_div{
      margin-left: 19px;
      margin-right: 19px;
    }
    .icon_box_c{
      background-color: #fff;
      border-radius: 180px;
      width: 133px;
      height: 133px;
      padding: 30px 10px 10px 31px;
      margin: 0px auto;
      border: 10px solid #F8FAFD;
    }
    .text_al{
      text-align: center;
    }
    .single_feature_h{
      height: 460px !important;
    }
    .rounded_gold{
      font-family: GothamRounded-Bold;
      font-size: 25px !important;
    }
    .rounded_book{
      font-family: GothamRounded-Book;
      font-size: 18px;
    }
    .fin_d{
      font-family: GothamRounded-Bold;
      font-size: 50px !important;
    }
    .rounded_medium{
      font-family: Gotham-Rounded-Medium;
      font-size: 19.5px;
    }
    .s_rounded_bok{
      font-family: GothamRounded-Book;
      font-size: 15.5px;
    }
    .gotop{
      font-family: GothamRounded-Book;
      font-size: 16px;
    }
    .check_c{
      font-family: Gotham-Rounded-Medium;
      font-size: 40px;
      text-align: left;
    }
    .check_text{
      font-family: GothamRounded-Book;
      font-size: 20px;
    }
    .single-trainer .meta-text{
      box-shadow:0px !important;
    }

    /*.carousel-item{min-height:300px; background:#ccc; }*/
    

    .carousel-control-next-icon, .carousel-control-prev-icon{
      color: #000;
    }
    .carousel-control-next, .carousel-control-prev{
          top: 89% !important;
        bottom: 0 !important;
    }
  </style>
@endpush

@section('content')
	 <!--================ Start Home Banner Area =================-->
    <section class="home_banner_area">
      <div class="banner_inner">
        <div class="container-fluid" style="position: absolute; margin-top: -180px;">
          <div class="row">
            <div class="clearfix"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-12">
              <div class="row">
                <div class="col-12">
                  <div class="banner_content text-center">
                    <h2 class="text-uppercase oab" style="">

                      Fond Your Doctor <br>
                      Easy to use search to find doctor for your specific need

                      {{-- Find a doctor online and book an appointment download our app now! --}}
                    </h2>
                    <h2 class="text-uppercase mt-4 mb-5" style="font-family: Gotham-Rounded-Medium; font-size: 17px; text-align: left;">
                      Download MyDoctorApp<br>
                      Find a Doctor Online<br>
                      Book An Appointment <br><br>
                      3-Easy-Steps
                      {{-- Book your local doctor in 3 simple and easy steps. --}}
                    </h2>
                  </div>
                </div>
              </div>
              <div class="col-12">
                  <a href="https://play.google.com/store/apps/details?id=com.nopso.pk.DoctorApp" target="blank">
                    <img src="{{asset('public/assets')}}/img/playstore.png" style="padding-right: 10px;">
                  </a>
                  <a href="https://itunes.apple.com/gb/app/mydoctor/id1445840119" target="blank">
                     <img src="{{asset('public/assets')}}/img/appstore.png">
                  </a>

               </div>
              </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-12">
              <img src="{{asset('public/assets/img')}}/3images.png" style="width: 100% !important;">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--================ End Home Banner Area =================-->

    <!--================ Start Feature Area =================-->
    <section class="feature_area section_gap_top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-12">
            <div class="single_feature single_feature_1">
              <div class="icon icon_box">
                <img src="{{asset('public/assets')}}/img/cal.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 rounded_gold">Book anytime</h4>
                <h5 class="rounded_book">
                  MyDoctor allows your patients to book their appointment 24 hours a day, 7 days a week. You can even get same-day appointments as well.

                </h5>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-12">
            <div class="single_feature single_feature_1">
              <div class="icon icon_box">
                <img src="{{asset('public/assets')}}/img/sear.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 rounded_gold">Find Your Doctor</h4>
                <h5 class="rounded_book">
                  Easy to use search to find doctor for your specific need.
                </h5>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-12">
            <div class="single_feature single_feature_1">
              <div class="icon icon_box">
                <img src="{{asset('public/assets')}}/img/clock.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 rounded_gold">Save Time and Money</h4>
                <h5 class="rounded_book">
                  Free up your staff from phone duties and allocate them to other needs. We save your time in finding a doctor and making an appointment.
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--================ End Feature Area =================-->

    <!--================ Start Feature Area =================-->
    <section class="feature_area section_gap_top">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="main_title">
              <h2 class="mb-3 text_al fin_d">Find Doctors</h2>
            </div>
          </div>
        </div>
        <div class="row" style="padding: 35px;">

          <div class="five_div"">
            <div class="single_feature single_feature_h">

              <div class="icon icon_box_c">
                <img src="{{asset('public/assets')}}/img/find_as.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 text_al rounded_medium">Find A Specialist</h4>
                <h5 class="text_al s_rounded_bok">You can easily search doctors by speciality</h5>
              </div>
              <div class="footer">
                <a href="" class="gotop">Go To Page <img src="{{asset('public/assets')}}/img/left_ar.png"></a>
              </div>
            </div>
          </div>
          <div class="five_div">
            <div class="single_feature single_feature_h">
              <div class="icon icon_box_c">
                <img src="{{asset('public/assets')}}/img/find_d.png" style="margin-top: -13px; margin-left: -8px;">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 text_al rounded_medium">Find Your Doctor</h4>
                <h5 class="text_al s_rounded_bok">
                  You can easily search doctors by City
                </h5>
              </div>
              <div class="footer">
                <a href="" class="gotop">Go To Page <img src="{{asset('public/assets')}}/img/left_ar.png"></a>
              </div>
            </div>
          </div>
          <div class="five_div">
            <div class="single_feature single_feature_h f_center">
              <div class="icon icon_box_c" style="border:10px solid #66BDE3;">
                <img src="{{asset('public/assets')}}/img/find_s.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 text_al rounded_medium" style="color: #fff;">Find doctor by name address, phone number</h4>
                <h5 class="text_al s_rounded_bok" style="color: #fff;">
                  You can easily search doctors by names, phone no and address
                </h5>
              </div>
              <div class="footer" style="padding-top: 28px; text-align: center;">
                <a href="" style="color: #fff;" class="gotop">Go To Page <i class="fa fa-angle-right fa-lg" style="color: #fff;"></i></a>
              </div>
            </div>
          </div>
          <div class="five_div" style="">
            <div class="single_feature single_feature_h">
              <div class="icon icon_box_c">
                <img src="{{asset('public/assets')}}/img/contact_d.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 text_al rounded_medium">Contact an online doctor</h4>
                <h5 class="text_al s_rounded_bok"> 
                  You can easily search doctors by names, phone no and address
                </h5>
              </div>
              <div class="footer" style="padding-top: 53px; text-align: center;">
                <a href="" class="gotop">Go To Page <img src="{{asset('public/assets')}}/img/left_ar.png"></a>
              </div>
            </div>
          </div>
          <div class="five_div">
            <div class="single_feature single_feature_h" style=" background-color: rgb(244,247,252);">
              <div class="icon icon_box_c">
                <img src="{{asset('public/assets')}}/img/precris.png">
              </div>
              <div class="desc">
                <h4 class="mt-3 mb-2 text_al rounded_medium">Get Prescription Now </h4>
                <h5 class="text_al s_rounded_bok">
                  You can easily search Get Prescription with senior doctor</h5>
              </div>
              <div class="footer" style="padding-top: 30px; text-align: center;">
                <a href="" class="gotop">Go To Page <img src="{{asset('public/assets')}}/img/left_ar.png"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

   

<!--container end.//-->

    <section class="trainer_area section_gap_top">
      <div class="container-fluid check_what">
        <div class="row justify-content-center d-flex align-items-center">
          <div class="col-lg-12  col-md-12 col-sm-12 col-12 single-trainer">
            <div class="thumb d-flex justify-content-sm-center">
              {{-- <img class="img-fluid" src="{{asset('public/assets')}}/img/trainer/t1.jpg" alt="" /> --}}
            </div>
            <div class="meta-text text-sm-center">
              <div class="row">
                <div class="col-md-4 col-sm-12" style="float: right;">
                  <div style="margin-top: 30px; background-color: rgb(0,144,206); width: 273px; height: 270px !important; margin-right: 50px;">
                    <img src="{{asset('public/assets')}}/img/dr.png" style="width: 100%; margin-left: 15px; margin-top: -30px;">
                  </div>
                </div>
                <div class="col-md-8 col-sm-12">
                   <div class="container-fluid sliders">
                    <aside class="col-md-12">
                        <div id="carousel2_indicator" class="carousel slide carousel-fade" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <h2 class="check_c">Check What our client say about us 1</h2>
                                      <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                      <h5 style="text-align: left;">
                                        Zeeshan Designer
                                      </h5>
                                      <h5 style="text-align: left;">Patient</h5>
                                </div>
                                <div class="carousel-item">
                                    <h2 class="check_c">Check What our client say about us 2</h2>
                                    <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                    <h5 style="text-align: left;">
                                      Zeeshan Designer
                                    </h5>
                                    <h5 style="text-align: left;">Patient</h5>
                                </div>
                                <div class="carousel-item">
                                    <h2 class="check_c">Check What our client say about us 3</h2>
                                    <h5 class="check_text" style="text-align: left;">I was in total shock of all the symptoms I had but my doctor never asked me to do a blood test. I was 36 and had no life. People including my doctor have accused me of illegal drug use or saying it’s all in my head </h5>
                                    <h5 style="text-align: left;">
                                      Zeeshan Designer
                                    </h5>
                                    <h5 style="text-align: left;">Patient</h5>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel2_indicator" role="button" data-slide="prev" style="bottom: 0 !important; margin-top: 46px; margin-left: -25px;">
                              <img src="{{asset('public/assets')}}/img/left_arrow.png">
                                <span class="sr-only">Previous</span>

                            </a>
                            <a class="carousel-control-next" href="#carousel2_indicator" role="button" data-slide="next" style="bottom: 0 !important; left: 37px !important; margin-top: 46px; margin-left: -25px;">
                              <img src="{{asset('public/assets')}}/img/right_arrow.png">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </aside>
                </div>

                  
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </section>

@endsection
	
@push('script')

@endpush