<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller
{
    //

    public function index(){
        return view('frontend/index');
    }
    public function searchby_city(Request $request){
        $city_name = $request->city_name;
        if($city_name != '' && $city_name != null){
          
            $dr = DB::select("select * from `salon` where `sal_search_words` like '%$city_name%' group by `sal_city` ");
        }else{
            $dr = DB::table('salon')->groupBy('sal_city')->paginate(20);
        }
    	return view('frontend/search-by-city',compact('dr','city_name'));
    }
    public function search_speciality(){
        $sp = DB::table('specialties')->get();
        return view('frontend/search-speciality',compact('sp'));
    }
    public function dr_detail($id){
        $dd = DB::table('salon')->select('sal_id','sal_name','sal_email','sal_phone','sal_degree','sal_rating','sal_description','sal_hours','sal_biography','sal_pic')->where('sal_id',$id)->first();
        // dd($dd);
        return view('frontend/dr_detail',compact('dd'));
    }
    public function see_dr(Request $request,$city){
        $doctor = $request->doctor;
        if($doctor != '' && $doctor != null){
          
            $dl = DB::select("select * from `salon` where `sal_search_words` like '%$doctor%' group by `sal_city` ");
        }else{
            $dl = DB::table('salon')->select('sal_id','sal_name','sal_phone','sal_rating','sal_phone','sal_degree','sal_pic','sal_profile_pic')->where('sal_city',$city)->get();
        }
        
        return view('frontend/see-dr',compact('doctor','dl','city'));
    }
    public function dr_ls($speciality){
        $city = '';
        $dl = DB::table('salon')->select('sal_id','sal_name','sal_phone','sal_rating','sal_phone','sal_degree','sal_pic','sal_profile_pic')->where('sal_specialty','%'.$speciality.'%')->orWhere('sal_search_words','%'.$speciality.'%')->get();
        return view('frontend/see-dr',compact('doctor','dl','city'));
    }
    public function prescription(){
        return view('frontend/prescription');
    }

    public function download(){
        return view('frontend/download');
    }

    public function contact(){
        return view('frontend/contact');
    }
    public function send_mail(Request $request){
        // dd($request->all());


        $to = "contactus@mydr.pk";
        $subject = $request->subject;
        
        $message = $request->email;
        $message .= "\r\n".$request->name;
        $message .= "\r\n".$request->message;
        $email = $request->email;
        $headers = 'From: contactus@mydr.pk' . "\r\n" .
            'Reply-To: contactus@mydr.pk' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $mails = mail($to, $subject, $message, $headers);

        // dd($mails);


        // $to = "fahimalyani73@gmail.com";
        // $from = $request->email;
        // $name = $request->name;
        // $subject = $request->subject;
        // $cmessage = $request->message;

        // $headers = "From: $from";
        // $headers = "From: " . $from . "\r\n";
        // $headers .= "Reply-To: " . $from . "\r\n";
        // $headers .= "MIME-Version: 1.0\r\n";
        // $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        // $subject = "You have a message from your Bitmap Photography.";

        // $logo = 'img/logo.png';
        // $link = '#';

        // $body = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Express Mail</title></head><body>";
        // $body .= "<table style='width: 100%;'>";
        // $body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
        // $body .= "<a href='{$link}'><img src='{$logo}' alt=''></a><br><br>";
        // $body .= "</td></tr></thead><tbody><tr>";
        // $body .= "<td style='border:none;'><strong>Name:</strong> {$name}</td>";
        // $body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
        // $body .= "</tr>";
        // $body .= "<tr><td style='border:none;'><strong>Subject:</strong> {$subject}</td></tr>";
        // $body .= "<tr><td></td></tr>";
        // $body .= "<tr><td colspan='2' style='border:none;'>{$cmessage}</td></tr>";
        // $body .= "</tbody></table>";
        // $body .= "</body></html>";
        // // dd($to, $body);
        // $send = mail($to, $subject, $body, $headers);

      return back()->with('message','Mail Send Successfully');

    }



    // $city=Request('city_title');
    //     $speciality=Request('speciality');
    //     $keyword=Request('keyword');
    //     $designation=Request('designation');
    //     if(!empty($city))
    //     {
    //      $data['jobslist']=Job::with('hospital:hospital_id,hospital_name,hospital_cover_img','city','username','specialities','area')->where('city_id',$city);
           
    //     }elseif(!empty($speciality))
    //     {
    //         $data['jobslist']=Job::with(['hospital:hospital_id,hospital_name,hospital_cover_img','city','username','area','specialities','jobdetail'=> function ($q) { $q->where('speciality_id',Request('speciality'));}]);
    //     }
    //     else
    //     {
    //      $data['jobslist']=Job::with('hospital:hospital_id,hospital_name,hospital_cover_img','city','username','specialities','area');  
    //     }
    //     $data['jobslist']->orderBy('id','desc')->get();
    //     dd($data);
}
